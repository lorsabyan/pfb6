'use strict';

console.log("==========================================================");
console.log("=== Programming Fundamentals for Beginners - Lesson 06 ===");
console.log("==========================================================");

// while loop
var i = 0;

while(i < 5) {
    console.log(i);
    i += 1;
}

// for loop
for(var i = 0; i < 5; i+= 1) {
    console.log(i);
}

var x = 1;

// increment
x = x + 1;
x += 1;
x++;

// decrement
x = x - 1;
x -= 1;
x--;

// postincrement vs preincrement
var a = 0;

console.log("a -> " + a);
console.log("a++ -> " + a++);
console.log("a -> " + a);
console.log("++a -> " + ++a);
console.log("a -> " + a);


console.log("a -> " + a);
console.log("a-- -> " + a--);
console.log("a -> " + a);
console.log("--a -> " + --a);
console.log("a -> " + a);

// Functions

function Max(a, b) {
    var max = a;
    
    if(b > max) {
        max = b;
    }
    
    return max;
}

var m1 = Max(2, 5);

var a = 7;
var b = 3;

var m2 = Max(a, b);

function PrintSeparatror() {
    console.log("-----------------------------");
}

function Greeting(name) {
    console.log("Hello " + name);
}

PrintSeparatror();
Greeting("John Smith");
PrintSeparatror();