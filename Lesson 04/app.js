'use strict';

console.log("==========================================================");
console.log("=== Programming Fundamentals for Beginners - Lesson 04 ===");
console.log("==========================================================");

// Գտնել երկու թվերից մեծագույնը։
// Գտնել երեք թվերից մեծագույնը։
var a = 12;
var b = 15;
var c = 7;
var max;

var max = a;

if (b > max) {
    max = b;
}

if (c > max) {
    max = c;
}

/* 
Ներմուծված տարիքից կախված արտածել, արդյո՞ք դիմորդը իրավունք ունի 
վարորդական իրավունքի քննություն հանձնելու, թե՞ ոչ։
*/

var age = 15;
var allowedAge = 17;

if (age >= allowedAge) {
    console.log("Yes");
}
else {
    console.log("No");
}


//********************************

var text = prompt("Age = ");
var year = parseInt(text);

if (isNaN(year)) {
    console.log("Non numberic value.")
}
else {
    if (year < 18) {
        alert('Not Allowed');
    }
    else {
        alert('Allowed');
    }
}

// *****************************
/* 
Արտածել համապատասխանաբար 
ցուրտ է (t <= 18 °C), 
հաճելի է (18 °C <  t < 24 °C), 
շոգ է (t >= 24 °C) կախված սենյակի ջերմաստիճանից: 
*/

var t = parseInt(prompt("t = "));

if(isNaN(t)) {
    alert("NaN value");
}
else {
    
    if(t <= 18) {
        alert("ցուրտ է");
    }
    
    if(t > 18 && t < 24) {
        alert("հաճելի է");
    }
    
    if(t >= 24) {
        alert("շոգ է");
    }
}