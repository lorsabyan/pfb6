'use strict';

console.log("==========================================================");
console.log("=== Programming Fundamentals for Beginners - Lesson 03 ===");
console.log("==========================================================");

// Հաշվել r շառավղով շրջանի մակերեսը օգտագործելով a = pi * r * r բանաձևը։
var r = 15;
// var a = Math.PI * Math.pow(r, 2);
var a = Math.PI * r * r;

console.log("radius = " + r);
//console.log("area = " + a);
//console.log("area = " + Math.round(a));
//console.log("area = " + a.toPrecision(5));
//console.log("area = " + a.toExponential(5));
console.log("area = " + a.toFixed(5));


/* 
Հաշվել r շառավղով շրջանագծի երկարությունը 
օգտագործելով c = 2 * pi * r բանաձևը։
*/
var r = 12.5;
var c = 2 * Math.PI * r;

console.log("radius = " + r);
console.log("c = " + c);

// Հաշվել, թե քանի վայրկյան կա n քանակությամբ օրերում։
var n = 7;
//var s = 24 * 60 * 60;
var secondsInDay = 86400;
var total = n * secondsInDay;

//console.log("seconds in a " + n + " days is equal to " + total);

/* ========================================================== */

var i = "day";

if(n > 1) {
    i = "days";
}

console.log("seconds in a " + n + " " + i + " is equal to " + total);


var answer = window.prompt("2 + 2 =");

if(answer == "4") {
    console.log("You are human.");
}
else {
    console.log("You are robot");
}

/*
>
<
>=
<=
== // not recomended
===
!= // not recomended
!==
 */

/*
&& - logical and
|| - logical or
!  - logical not
 */

// 2 >= 1 equivalent to 2 > 1 || 2 === 1
// t > 18 && t < 25

var x = 5;
x = x + 2;
x += 2;
x -= 2;

/*
+=
-=
*=
/=
 */

x *= 2;
x /= 2;