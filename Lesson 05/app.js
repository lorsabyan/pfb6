'use strict';

console.log("==========================================================");
console.log("=== Programming Fundamentals for Beginners - Lesson 05 ===");
console.log("==========================================================");

// array declaration
var array = [2, 5, -8, 50, 7, 11, 0, 15];

var i = 0;

// while loop
while(i < array.length) {
    
    console.log(i + ": " + array[i]);
    i = i + 1;
    
}

var max = array[0]; // first element of array
i = 1;

// array.length is the number of elements in array
while(i < array.length) {
    
    if(array[i] > max) {
        max = array[i];
    }
    
    i += 1;
    
}

console.log("max = " + max);

/****************** */
// sum of array elements

i = 0;
var sum = 0;

while(i < array.length) {
    sum += array[i];
    i += 1;
}

console.log("sum = " + sum);
console.log("avr = " + sum / array.length);

var h = 0;
var m = 0;

while(h < 24) {
    
    while(m < 60) {
        console.log(h + ":" + m);
        m += 1;
    }
    
    h += 1;
    m = 0;
}