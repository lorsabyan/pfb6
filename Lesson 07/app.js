'use strict';

console.log("==========================================================");
console.log("=== Programming Fundamentals for Beginners - Lesson 07 ===");
console.log("==========================================================");

function ArrayMax(array) {
    var max = array[0];
    for (var i = 1; i < array.length; i++) {
        if(array[i] > max) {
            max = array[i];
        }
    }
    return max;
}

function ValueIndexes(array, value) {
    var indexes = [];
    for(var i = 0; i < array.length; i++) {
        if(array[i] === value) {
            indexes.push(i);
        }
    }
    return indexes;
}

var array = [2, 0, 5, 5, 3, 5, 2, 5, 3, 0];

var max = ArrayMax(array);
var indexes = ValueIndexes(array, max);

console.log(array);
console.log(indexes);

function GetUnitRandomArray(n) {
    var array = [];
    for(var i = 0; i < n; i++) {
        array.push(Math.random());
    }
    
    return array;
}

console.log(GetUnitRandomArray(50));

function GetRandom(min, max) {
    return min + Math.random() * (max - min);
}

function GetIntRandom(min, max) {
    var r = min + Math.random() * (max - min);
    return Math.round(r);
}

function GetRandomArray(n, min, max) {
    var array = [];
    for(var i = 0; i < n; i++) {
        array.push(GetRandom(min, max));
    }
    
    return array;
}

function GetIntRandomArray(n, min, max) {
    var array = [];
    for(var i = 0; i < n; i++) {
        array.push(GetIntRandom(min, max));
    }
    
    return array;
}

console.log(GetRandomArray(50, 1, 100));
console.log(GetIntRandomArray(50, 1, 100));

var array = GetIntRandomArray(5, 1, 100);

console.log(array);
console.log(ArrayMax(array));

function Dice() {
    var number = Math.round(1 + 5 * Math.random());
    var p = document.getElementById("number");
    p.innerHTML = number;
}

