'use strict';

console.log("==========================================================");
console.log("=== Programming Fundamentals for Beginners - Lesson 01 ===");
console.log("==========================================================");

// Greeting in browser console
console.log("Hello World!");

// Greeting in html document
document.write("<p>Hello World</p>");