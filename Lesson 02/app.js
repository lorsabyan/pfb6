'use strict';

console.log("==========================================================");
console.log("=== Programming Fundamentals for Beginners - Lesson 02 ===");
console.log("==========================================================");


var name = "John Smith";
var age = 30;

console.log("Name: " + name);
console.log("Age: " + age);

console.log("----------------------------------------------------------");

var a = 5;
var b = 85;

var p = (a / b) * 100;

console.log(a + " is a " + p + "% of " + b);

console.log("----------------------------------------------------------");