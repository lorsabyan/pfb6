'use strict';

console.log("==========================================================");
console.log("=== Programming Fundamentals for Beginners - Lesson 08 ===");
console.log("==========================================================");

var i = 5;

while (i < 5) {
    console.log(i);
    i++;
}

var i = 5;

do {
    console.log(i);
    i++;
} while (i < 5);

/******************* */

function GetQuantity() {
    var inputNumber = document.getElementById("quantity");
    var quantity = parseInt(inputNumber.value);
    return quantity;
}

function OutputResult(message) {
    var p = document.getElementById("result");
    p.innerHTML = message;
}

function Sum() {
    var quantity = GetQuantity();

    if (isNaN(quantity)) {
        alert("Please input quantity.");
        return;
    }

    var numbers = [];
    var n;

    do {
        n = parseInt(prompt("n = "));

        if (!isNaN(n) || isFinite(n)) {
            numbers.push(n);
        }
    } while (numbers.length < quantity)

    var s = 0;

    for (var i = 0; i < numbers.length; i++) {
        s += numbers[i];
    }

    var message = numbers.join(" + ");

    OutputResult(message + " = " + s);
}


/** switch */

var n = parseInt(prompt("day number = "));

if (n === 1) {
    alert("Mo");
}

if (n === 2) {
    alert("Tu");
}

switch (n) {
    case 1:
        alert("Mo");
        break;
    case 2:
        alert("Tu");
        break;
    case 6:
    case 7:
        alert("Weekend");
        break;
    default:
        alert("Unknown");
}

for (var number = 0; number < 10; number++) {
    if(number % 2 == 0) {
        continue;
    }   
    
    if(number % 25 == 0) {
        break;
    }
    
    console.log(number); 
}